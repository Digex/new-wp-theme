<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php wp_title(''); ?></title>
	<link rel="icon" type="image/png" href="<?php echo bloginfo('template_url'); ?>/assets/img/OventionFavicon.ico?1234">
	<link rel="apple-touch-icon" href="<?php echo bloginfo('template_url'); ?>/assets/img/apple-touch-icon.png">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<?php get_template_part( 'partials/partial', 'ga' ); ?>
</head>
<body <?php body_class(); ?>>
<?php
$header = get_field('header', 'option'); 
?>
<div class="top-header">
	<div class="row">
		<div class="large-5 columns top-header__testimonies">
			<?php $rows = $header['testimonies_slider'];
						if($rows): 
						?>
							<?php foreach($rows as $row): 
								$testimony_text = $row['testimony_text'];
								$testimony_logo = $row['testimony_logo'];
							?>
							   <div class="top-header__single-testimony">
									<p><?= $testimony_text; ?></p><span>—</span><img src="<?= $testimony_logo['url']; ?>" alt="<?= $testimony_logo['alt']; ?>">
								</div>
							<?php endforeach; ?>
			<?php	endif; ?>
			
		</div>
		<div class="large-7 columns hide-for-small show-for-large text-right">
		        <div class="top-header__navigation">
		        	<ul class="menu text-left" data-responsive-menu="dropdown large-dropdown">
		              <?php
		                  $defaults = array(
		                    'theme_location'  => 'header_top_menu',
		                    'menu'            => '',
		                    'container'       => '',
		                    'container_class' => '',
		                    'container_id'    => '',
		                    'menu_class'      => '',
		                    'menu_id'         => '',
		                    'echo'            => true,
		                    'fallback_cb'     => 'wp_page_menu',
		                    'before'          => '',
		                    'after'           => '',
		                    'link_before'     => '',
		                    'link_after'      => '',
		                    'items_wrap' => '%3$s',
		                    'depth'           => 0,
		                    'walker'        => new themeslug_walker_nav_menu_header
		                  );
		                wp_nav_menu($defaults); ?>
		            </ul>
		        </div>
		        <div class="top-header__social">
					<?php $rows = $header['header_social'];
						if($rows): 
						?>
							<?php foreach($rows as $row): 
								$social_url = $row['social_url'];
								$social_icon = $row['social_icon'];
							?>
							   <a href="<?= $social_url; ?>" target="_blank" rel="nofollow"><i class="<?=$social_icon; ?>"></i></a>
							<?php endforeach; ?>
				<?php	endif; ?>
				</div>
		</div>
	</div>
</div>
<div class="bottom-header">
	<div class="row">
		<div class="large-3 columns bottom-header__logo hide-for-small show-for-large">
			<a href="<?php echo esc_url( home_url( '/' )); ?>"><img src="<?= $header['header_logo']['url']; ?>" alt="<?= $header['header_logo']['alt']; ?>"></a>
		</div>
		<div class="large-9 columns bottom-header__navigation">
			<div class="title-bar hide-for-large" data-responsive-toggle="responsive-menu" data-hide-for="large">
	          <div class="title-bar-title text-center">
	          	<a href="<?php echo esc_url( home_url( '/' )); ?>"><img src="<?php bloginfo('template_url');?>/assets/img/header-logo.png" alt=""></a>
	          </div>
	          <button class="menu-icon" type="button" data-toggle>
	          	<i class="fas fa-bars"></i>
	          </button>
	        </div>
	        <div class="top-bar" id="responsive-menu">
	            <ul class="menu text-left large-text-right" data-responsive-menu="accordion large-dropdown" data-parent-link="true">
	              <?php
	                  $defaults = array(
	                    'theme_location'  => 'header_main_menu',
	                    'menu'            => '',
	                    'container'       => '',
	                    'container_class' => '',
	                    'container_id'    => '',
	                    'menu_class'      => 'navigation',
	                    'menu_id'         => '',
	                    'echo'            => true,
	                    'fallback_cb'     => 'wp_page_menu',
	                    'before'          => '',
	                    'after'           => '',
	                    'link_before'     => '',
	                    'link_after'      => '',
	                    'items_wrap' => '%3$s',
	                    'depth'           => 0,
	                    'walker'        => new themeslug_walker_nav_menu_header
	                  );
	                wp_nav_menu($defaults); ?>
	            </ul>
	          </div>
		 </div>
	</div>
</div>
