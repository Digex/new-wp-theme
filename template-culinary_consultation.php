<?php
/*
*Template Name: Culinary Consultation
*/
get_header();
$culinary_consultation = get_field('culinary_consultation');
?>

<!-- BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!-- /BANNER -->
<!-- FULL PAGE -->
<div class="full-page cloud-pattern">
	<div class="row">
		<div class="columns small-12 white-box">
			<div class="small-8 small-centered ">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
				<?php endwhile;  endif; ?>
			    <div class="culinary-icons" data-equalizer>
			    	<h4><?=$culinary_consultation['title']; ?></h4>
			    	<?php $rows = $culinary_consultation['icons'];
						if($rows): 
						?>
							<?php foreach($rows as $row): 
								$icon = $row['icon'];
								$description = $row['description'];
							?>
								<div class="culinary-icons__single" data-equalizer-watch>
						    		<img src="<?=$icon['url']; ?>" alt="<?=$icon['alt']; ?>">
						    		<?=$description; ?>
						    	</div>
							<?php endforeach; ?>
					<?php	endif; ?>
			    </div>
				<?=$culinary_consultation['form']; ?>
			</div>

		</div>
	</div>

</div>
<!-- /FULL PAGE -->

<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->

<?php get_footer(); ?>
