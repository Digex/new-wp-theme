<?php
/*
*Template Name: Library
*/

get_header();
$library    = get_field('library');
$spec_sheet = $library['spec_sheet'];
$manuals    = $library['manuals'];
$brochures  = $library['brochures'];
$ventless   = $library['ventless'];
?>

<!-- BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!-- /BANNER -->

<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->

<!-- FULL PAGE -->
<div class="full-page cloud-pattern">
	<div class="row">
		<div class="columns small-12 white-box">
			<div class="small-10 small-centered ">
			<?php $rows = $spec_sheet['files'];
				if($rows): ?>
				<div id="spec_sheet" class="library-content">
					<h2><?=$spec_sheet['title']; ?></h2>
					<?php foreach($rows as $row): 
						  $label = $row['label'];
						  $file  = $row['file'];
					?>
						<div class="library-content__link">
							<label><?=$label; ?></label> <a href="<?=$file['url']?>" download="">Download</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;?>
			<?php $rows = $manuals['files'];
				if($rows): ?>
				<div id="manuals" class="library-content">
					<h2><?=$manuals['title']; ?></h2>
					<?php foreach($rows as $row): 
						  $label = $row['label'];
						  $file  = $row['file'];
					?>
						<div class="library-content__link">
							<label><?=$label; ?></label> <a href="<?=$file['url']?>" download="">Download</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;?>
			<?php $rows = $brochures['files'];
				if($rows): ?>
				<div id="brochures" class="library-content">
					<h2><?=$brochures['title']; ?></h2>
					<?php foreach($rows as $row): 
						  $label = $row['label'];
						  $file  = $row['file'];
					?>
						<div class="library-content__link">
							<label><?=$label; ?></label> <a href="<?=$file['url']?>" download="">Download</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;?>
			<?php $rows = $ventless['files'];
				if($rows): ?>
				<div id="ventless" class="library-content">
					<h2><?=$ventless['title']; ?></h2>
					<?php foreach($rows as $row): 
						  $label = $row['label'];
						  $file  = $row['file'];
					?>
						<div class="library-content__link">
							<label><?=$label; ?></label> <a href="<?=$file['url']?>" download="">Download</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;?>
			</div>
		</div>
	</div>

</div>
<!-- /FULL PAGE -->

<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->

<?php get_footer(); ?>
