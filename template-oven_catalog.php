<?php
/*
*Template Name: Oven Catalog
*/
get_header(); 

?>
<!--  BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!--  /BANNER -->
<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->

<div class="catalog-oven cloud-pattern">
	<div class="row">
		<div class="small-11 small-centered columns text-center">
			<?php if ( have_posts() ) : 
					while ( have_posts() ) : the_post();
						the_content();
					endwhile; 
				  else: ?>
					<p>Sorry, no posts matched your criteria.</p>
			<?php endif; ?>
		</div>
	</div>
	<div class="catalog-oven__container">
	 <?php
	   $args = array(
		    'post_type'      => 'page',
		    'posts_per_page' => -1,
		    'post_parent'    => $post->ID,
		    'order'          => 'ASC',
		    'orderby'        => 'menu_order'
		 );
	   $query = new WP_Query( $args );
        if($query -> have_posts() ):
            while ($query -> have_posts()):
                  $query -> the_post();
                  $id = get_the_id();
                  $mini_data              = (get_field('mini_data',$id)) ? get_field('mini_data',$id) : '' ;
              ?>
				<div class="row">
					<div class="small-12 columns relative" >
					   <?php if( $mini_data['is_new']):?>
					   		<img src="<?php bloginfo('template_url') ?>/assets/img/new-bar.png" alt="New" title="New" class="catalog-oven__new">
					   <?php endif; ?>
					   <div class="catalog-oven__single" data-equalizer>
							<div class="large-6 columns large-push-6 catalog-oven__text" data-equalizer-watch>
								<a href="<?php the_permalink(); ?>" class="catalog-oven__link"><?=$mini_data['mini_title']?></a>
								<?=$mini_data['mini_description']?>
								<!-- <a href="<?php the_permalink(); ?>" class="catalog-oven__button ovention-button ovention-button--colors">More info &raquo;</a> -->
							</div>
							<div class="large-6 columns text-center catalog-oven__image large-pull-6" data-equalizer-watch>
								<a href="<?php the_permalink(); ?>"><img src="<?=$mini_data['mini_oven_image']['url']?>" alt="<?=$mini_data['mini_oven_image']['alt']?>"></a>
							</div>
							
						</div>
					</div>
				</div>
               <?php
            endwhile;
        endif; ?>
	</div>
</div>

<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->
<?php get_footer(); ?>