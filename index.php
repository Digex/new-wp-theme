<?php
get_header();
?>

<!-- BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!-- /BANNER -->

<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->

<!-- FULL PAGE -->
<div class="full-page cloud-pattern">
	<div class="row">
		<div class="columns small-12 white-box">
			<div class="small-10 small-centered ">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
				<?php endwhile;  endif; ?>
			</div>

		</div>
	</div>

</div>
<!-- /FULL PAGE -->

<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->

<?php get_footer(); ?>
