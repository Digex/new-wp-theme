<?php
add_action( 'init', 'create_post_type_calculator' );
function create_post_type_calculator() {
  register_post_type( 'calculator',
    array(
      'labels' => array(
        'name' => __( 'Calculator' ),
        'singular_name' => __( 'Calculator' ),
        'add_new' => __( 'Add New Oven' ),
        'edit_item' => __( 'Edit Oven' ),
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'calculator'),
      'menu_icon'   => 'dashicons-admin-settings',
      'supports' => array('title'),
      'menu_position' => 6,
    )
  );
}

