<?php
/*
Plugin Name: Post type videos
Plugin URI: 
Description: Post type videos
Author: .
Version: 1.0.1
Author URI: 
*/

add_action( 'init', 'category_videos_tax' );

function category_videos_tax() {
    register_taxonomy(
        'category-videos',
        'videos',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'category-videos' ),
            'hierarchical' => true,
        )
    );
}

add_action( 'init', 'create_post_type_videos' );
function create_post_type_videos() {
    register_post_type( 'videos',
        array(
            'labels' => array(
                'name' => __( 'Videos' ),
                'singular_name' => __( 'videos' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'videos'),
            'supports' => array('title', 'page-attributes', 'editor','excerpt', 'post-thumbnails', 'thumbnail'),
            'taxonomies' => array('category-videos'),  
        )
    );
}
// Custom Post Type: Featured Items: Appearance

