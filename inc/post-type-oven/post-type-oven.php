<?php
/*
Plugin Name: Post type oven
Plugin URI: 
Description: Post type oven
Author: .
Version: 1.0.1
Author URI: 
*/

add_action( 'init', 'category_oven_tax' );

function category_oven_tax() {
    register_taxonomy(
        'category-oven',
        'oven',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'category-oven' ),
            'hierarchical' => true,
        )
    );
}

add_action( 'init', 'create_post_type_oven' );
function create_post_type_oven() {
    register_post_type( 'oven',
        array(
            'labels' => array(
                'name' => __( 'Ovens' ),
                'singular_name' => __( 'oven' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'oven'),
            'supports' => array('title', 'page-attributes', 'editor','excerpt', 'post-thumbnails', 'thumbnail'),
            'taxonomies' => array('category-oven'),  
        )
    );
}
// Custom Post Type: Featured Items: Appearance
