<?php
/*
*Template Name: Ovens
*/
get_header();
$oven = get_field('oven');
?>
<!--  BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!--  /BANNER -->

<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->
<!--  FIRTS SECTION -->
<div class="intro-oven cloud-pattern">
  <div class="row">
      <div class="small-12 columns">  
        <?php the_post_thumbnail( 'full' ); ?>
      </div>
  </div>
</div>
<div class="main-oven orange-overlay">
	<div class="row">
		<div class="large-5 columns">
			<?php if ( have_posts() ) : 
					while ( have_posts() ) : the_post();
						the_content();
					endwhile; 
				  else: ?>
					<p>Sorry, no posts matched your criteria.</p>
			<?php endif; ?>
		</div>
		<div class="large-5 large-offset-2 columns">
			<div class="main-oven__box">
			<?php 
			if($oven['oven_vimeo_id'] != null):
			?>
				<div class="main-oven__video">
					<div class="vimeo-player" data-id="<?= $oven['oven_vimeo_id']; ?>"></div>
				</div>
				<div class="main-oven__links">
				    <h3><?= $oven['oven_links_title']; ?></h3>
				    <?php $rows = $oven['oven_links'];
							if($rows): 
							?>
								<?php foreach($rows as $row): 
									$link_type = $row['link_type'];
									$video_link = $row['video_link'];
									$attachment = $row['attachment'];
									$text_link = $row['text_link'];
								?>
										<div class="row main-oven__row">
										<?php if($link_type=='download'): ?>
											<div class="main-oven__icon">
												<i class="fas fa-cloud-download-alt"></i>
											</div>
											<div class="main-oven__attached">
												<a href="<?= $attachment['url'] ?>" download=""><?=$text_link;  ?></a>
											</div>
										<?php else: ?>
											<div class="main-oven__icon">
												<i class="fas fa-play-circle"></i>
											</div>
											<div class="main-oven__attached">
												<a href="<?=$video_link; ?>"><?=$text_link;  ?></a>
											</div>
										<?php endif; ?>
										</div>
									
								<?php endforeach; ?>
					<?php	endif; ?>
				</div>
				<?php	endif; ?>
			</div>
		</div>
	</div>
</div>
<?php 
if($oven['oven_ribbons'] != null):
?>
<div class="oven-ribbons hide-for-small show-for-medium">
	<div class="oven-ribbons__close"><i class="fas fa-times-circle"></i></div>
	<?php $rows = $oven['oven_ribbons'];
			if($rows): ?>
				<?php foreach($rows as $row): 
					$ribbon_image = $row['ribbon_image'];
				?>
				<img src="<?= $ribbon_image['url'] ?>" alt="<?= $ribbon_image['alt'] ?>">
				<?php endforeach; ?>
	<?php	endif; ?>
</div>
<?php endif; ?>
<!--  /FIRTS SECTION -->
<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->
<?php get_footer(); ?>
