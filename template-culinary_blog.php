<?php
/*
*Template Name: Culinary Blog
*/
 get_header() ?>
<!--  BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!--  /BANNER -->
<div class="cloud-dark-pattern">
	<div class="row relative">
		<div class="small-12 columns featured-slider">
		<?php
          	global $wp_query;
           	$wp_query = new WP_query(array('post_type' => 'post', 'posts_per_page' => -1)); 
				if ( $wp_query-> have_posts() ) : 
					while ( $wp_query->have_posts() ) : $wp_query->the_post();
					$featured = get_field('is_a_featured_post');
					if($featured):
				    $featured_img = get_field('featured_image'); 
				?>
				<div class="featured-slider__single" data-equalizer>
					<div class="large-6 columns featured-slider__image no-padding" data-equalizer-watch>
						<a href="<?php the_permalink(); ?>">
						<?php if($featured_img): ?>
							<img src="<?= $featured_img['url']; ?>">
					    <?php else: ?>
					    	<img src="<?php bloginfo('template_url' ) ?>/assets/img/slider-placeholder.jpg">
					    <?php endif; ?>
					   </a>
					</div>
					<div class="large-6 columns featured-slider_content" data-equalizer-watch>
						 <div class="archive-content__excerpt">
	   	    				 	<h4><?php the_title(); ?></h4>
	   	    				    <div class="archive-content__info">
	   	    				 		<div class="archive-content__info-single archive-content__info-single--date">
	   	    				 			<?php echo get_the_date(); ?>
	   	    				 		</div>
	   	    				 		<div class="archive-content__info-single archive-content__info-single--category">
   	    				 			  <?php $categories = get_the_category();
										if ( ! empty( $categories ) ):
										    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
										endif; ?>
	   	    				 		</div>
	   	    				 	</div>
	   	    					<?php the_excerpt(); ?>
	   	    					<a href="<?php the_permalink(); ?>">Read More &raquo;</a>
	   	    				 </div>
	   	    				 
					</div>
				</div>
	 				<?php
	 				endif;
          		endwhile;
          	endif;
            wp_reset_postdata();
           ?>

		</div>
		 <div class="clean"></div>
		 <div class="dots_custom"></div>
	</div>
	<div class="archive-content">
	   <div class="row">
	   	    <div class="large-8 columns">
	   	    	<div class="archive-content__grid">
                <?php
                  	global $wp_query;
                   	$wp_query = new WP_query(array('post_type' => 'post', 'paged'=>$paged)); 
 					if ( $wp_query-> have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
                    <div class="archive-content__single" data-w="2" >
	   	    			<div>
	   	    				<div class="archive-content__thumbnail">
	   	    				  <?php
                            	if ( has_post_thumbnail() ): ?>
                                 <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
                               <?php 
                                else:  ?>
                                <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url' ) ?>/assets/img/press-default.jpg" /></a>
                               <?php endif; ?>	
	   	    				</div>
	   	    				 <div class="archive-content__excerpt">
	   	    				 	<h4><?php the_title(); ?></h4>
	   	    				    <div class="archive-content__info">
	   	    				 		<div class="archive-content__info-single archive-content__info-single--date">
	   	    				 			<?php echo get_the_date(); ?>
	   	    				 		</div>
	   	    				 		<div class="archive-content__info-single archive-content__info-single--category">
   	    				 			  <?php $categories = get_the_category();
										if ( ! empty( $categories ) ):
										    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
										endif; ?>
	   	    				 		</div>
	   	    				 	</div>
	   	    					<?php the_excerpt(); ?>
	   	    					<a href="<?php the_permalink(); ?>">Read More &raquo;</a>
	   	    				 </div>
	   	    			</div>
	   	    		</div>
                <?php
                  		endwhile;
                  	endif;
                     wp_reset_postdata();
                  ?></div>
	   	    </div>
			<div class="large-4 columns">
				<!--  CATEGORY SIDEBAR -->
				<?php get_template_part( 'partials/content', 'category_sidebar' ); ?>
				<!--  /CATEGORY SIDEBAR -->
			</div>
	   </div>
       <?php get_template_part( 'partials/content', 'paginator' ); ?>
	</div>
</div>
<?php get_footer(); ?>