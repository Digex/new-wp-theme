<?php
/*
*Template Name: Why Ovention
*/

get_header();
$why_ovention = get_field('why_ovention');
?>

<!-- BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!-- /BANNER -->

<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->
<div class="why-ovention cloud-pattern" data-equalizer>
	<div class="row">
		<div class="large-6 columns why-ovention__text" data-equalizer-watch>
		<div class="why-ovention__button-content">
		<?php $rows = $why_ovention['why_ovention_buttons'];
					if($rows): 
					?>
						<?php foreach($rows as $row): 
							$button = $row['why_ovention_button'];
						?>
							<a href="<?=$button['url'] ?>" target="<?=$button['target'] ?>" class="why-ovention__button ovention-button ovention-button--colors"><?=$button['title'] ?></a>
						<?php endforeach; ?>
			<?php	endif; ?>
			</div>
		</div>
		<div class="large-6 columns why-ovention__image" data-equalizer-watch>
			<?php the_post_thumbnail( 'full' ); ?>
		</div>
	</div>
</div>
<div  class="complementary orange-overlay">
	<div class="row">
		<div class="large-5 columns">
			<?= $why_ovention['why_ovention_left_content']; ?>
		</div>
		<div class="large-5 columns large-offset-2">
			<?= $why_ovention['why_ovention_right_content']; ?>
		</div>
	</div>
</div>
<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->

<?php get_footer(); ?>