<?php
/*
*Template Name: About Us
*/

get_header();
$history = get_field('history');
$corporate_statement = get_field('corporate_statement');
$made_in_the_usa = get_field('made_in_the_usa');
$staff = get_field('staff');
?>
<!--  BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!--  /BANNER -->
<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->
<div class="history-about cloud-pattern">
	<div class="row">
		<div class="large-7 columns history-about__text">
			<h2><?=$history['history_title']; ?></h2>
			<?=$history['history_content']; ?>
		</div>
		<div class="large-5 columns history-about__image">
			<img src="<?=$history['history_image']['url']; ?>" alt="<?=$history['history_image']['alt']; ?>">
		</div>
	</div>
</div>
<div class="values-about orange-overlay">
	<div class="row">
		<div class="large-5 columns">
			<?=$corporate_statement['corporate_statement_left_content']; ?>
		</div>
		<div class="large-5 large-offset-2 columns">
			<?=$corporate_statement['corporate_statement_right_content']; ?>
		</div>
	</div>
</div>
<div class="made-box">
	<div class="row">
		<div class="large-6 large-offset-6 columns">
			<h2><?=$made_in_the_usa['title']; ?></h2>
			<h5><?=$made_in_the_usa['subtitle']; ?></h5>
			<?=$made_in_the_usa['first_content']; ?>
			<div class="show-for-small hide-for-large">
				<?=$made_in_the_usa['second_content']; ?>
			</div>
		</div>
	</div>
</div>
<div class="cloud-pattern">
	<div class="second-made-box hide-for-small show-for-large">
		<div class="row">
			<div class="large-6 columns second-made-box__image">
				<img src="<?=$made_in_the_usa['image']['url']; ?>" alt="<?=$made_in_the_usa['image']['alt']; ?>">
			</div>
			<div class="large-6 columns second-made-box__text">
				<?=$made_in_the_usa['second_content']; ?>
			</div>
		</div>
	</div>
	<div class="staff">
		<div class="row staff__intro">
			<div class="large-8 columns large-centered text-center">
				<h2><?=$staff['staff_title']; ?></h2>
				<?=$staff['staff_intro']; ?>
			</div>
		</div>
		<div class="row staff__members text-center" data-equalizer>
		  <?php $rows = $staff['staff_members'];
					if($rows): 
					?>
						<?php foreach($rows as $row): 
							$staff_image       = $row['staff_image'];
							$staff_name        = $row['staff_name'];
							$staff_charge      = $row['staff_charge'];
							$staff_description = $row['staff_description'];
						?>
						<div class="staff__single-member">
							<div class="staff__box" data-equalizer-watch>
								<img src="<?=$staff_image['url']; ?>" alt="<?=$staff_image['alt']; ?>">
								<h4><?=$staff_name; ?></h4>
								<h6><?=$staff_charge; ?></h6>
								<?=$staff_description; ?>
							</div>
						</div>
						<?php endforeach; ?>
			<?php	endif; ?>
		</div>
	</div>
</div>
<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->
<?php get_footer(); ?>
