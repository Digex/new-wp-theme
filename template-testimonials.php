<?php
/*
*Template Name: Testimonials
*/
get_header();
$testimonials = get_field('testimonials');
?>
<!--  BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!--  /BANNER -->
<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->
<div class="testimonials cloud-pattern">
	<div class="testimonials__container">
		<div class="row">
			<div class="small-12 columns relative" >
			<?php $rows = $testimonials;
			if($rows): 
			?>
				<?php foreach($rows as $row): 
					$testimonial_image = $row['testimonial_image'];
					$testimonial_title = $row['testimonial_title'];
					$testimonial_content = $row['testimonial_content'];
					$testimonial_author = $row['testimonial_author'];
					$testimonial_file = $row['testimonial_file'];
				?>
				    <div class="testimonials__single" data-equalizer>
				   		<div class="large-4 columns text-center testimonials__image" data-equalizer-watch>
							<a href="<?= $testimonial_file['url']; ?>"><img src="<?= $testimonial_image['url']; ?>" alt="<?= $testimonial_image['alt']; ?>"></a>
						</div>
						<div class="large-8 columns testimonials__text" data-equalizer-watch>
							<h3><?= $testimonial_title; ?></h3>
							<?= $testimonial_content; ?>
							<span><?= $testimonial_author; ?></span>
							<a href="<?= $testimonial_file['url']; ?>" class="testimonials__button ovention-button ovention-button--colors">Download</a>
						</div>
					</div>
					
				<?php endforeach; ?>
			<?php	
			endif; ?>
			</div>
		</div>
	</div>
</div>
<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->
<?php get_footer(); ?>
