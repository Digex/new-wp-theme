<?php
/*
*Template name: Calculator
*/
get_header(); ?>
<!-- BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!-- /BANNER -->

<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->

<!-- FULL PAGE -->
<div class="full-page calculator cloud-pattern">
  <div class="row">
    <div class="columns small-12 white-box">
      <div class="small-12 medium-8 small-centered columns">
        <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                  <?php the_content( );?>
                <?php endwhile; ?>
              <?php endif; ?>
              <?php
                $args = array(
                  'post_type' => 'calculator',
                  'posts_per_page' => -1,
                );
                $query = new WP_Query($args);
              ?>
              <select name="" id="ovenpost">
                <option value="0">Choose Ovention Model</option>
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                  <option value="<?php the_ID(); ?>"><?php the_title(); ?></option>
                <?php
                  endwhile;
                  wp_reset_postdata();
                ?>
              </select>
              <select name="" id="pizza">
                <option value="0">Choose Pizza Type</option>
                <?php
                  $field = get_field_object('field_58d418375e8dc');
                  foreach( $field['choices'] as $k => $v ){
                    echo '<option value="' . $k . '">' . $v . '</option>';
                  }
                ?>
              </select>

              <div class="responsehtml">

              </div>

      </div>

    </div>
  </div>

</div>
<!-- /FULL PAGE -->
<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->

<?php get_footer(); ?>

