<?php 
get_header();
$archive_videos = get_field('archive_videos', 'option'); 
?>
<div class="general-banner">
	<div class="row">
		<div class="small-12 columns text-center">
			<h1><?= $archive_videos['title']?></h1>
			<?= $archive_videos['content']?>
		</div>
	</div>
</div>
<!-- CONTENT -->
<?php get_template_part( 'partials/content', 'archive_videos' ); ?>
<!-- /CONTENT -->          
<?php get_footer(); ?>