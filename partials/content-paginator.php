<div class="clean"></div>
<div class="row">
	<div class="pagination-block large-12 columns">
		<?php $max_page = $wp_query->max_num_pages;
		if (!$paged && $max_page >= 1) {
		    $current_page = 1;
		}
		else {
		    $current_page = $paged;
		} ?>
		<div class="pagination-block__pagination">
			<span class="page-index"></span>
			<?php pagination('Prev', 'Next'); ?>
		</div>
	</div>
</div>