<?php
$culinary_bar = get_field('culinary_bar', 'option'); 
?>
<div class="blog-bar">
	<div class="row blog-bar__content">
		<div class="medium-6 large-6 medium-offset-3 large-offset-3 columns">
			<h2><?= $culinary_bar['culinary_bar_title'];?></h2>
			<?= $culinary_bar['culinary_bar_content'];?>
		</div>
		<div class="medium-3 large-3 columns">
			<a href="<?= $culinary_bar['culinary_bar_button']['url'];?>" class="blog-bar__button ovention-button ovention-button--white"><?= $culinary_bar['culinary_bar_button']['title'];?> &raquo;</a>
		</div>
	</div>
</div>