<div class="cloud-dark-pattern">
	<div class="archive-content">
	   <div class="row">
	   	    <div class="large-8 columns">
	   	    	<div class="archive-content__grid">
                <?php
                    global $wp_query;
 					if ( $wp_query-> have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
 					$video = get_field('video_id');
 					?>
                    <div class="archive-content__single" >
	   	    			<div>
	   	    				<div class="archive-content__thumbnail">
								<div class="vimeo-thumb" data-id="<?= $video; ?>" data-open="V<?=$video; ?>"></div>
	   	    				</div>
	   	    				 <div class="archive-content__excerpt archive-content__excerpt--videos">
	   	    				 	<?php	$term_list = wp_get_post_terms($post->ID, 'category-videos', array("fields" => "all"));
										if($term_list): ?>
									    	<a href="<?php  echo get_category_link( $term_list[1]->term_id ); ?>">
									    			<?php echo $term_list[1]->name; ?>
									    	</a>
								<?php   else:?>
											<a href="<?php echo site_url(); ?>/videos/">All</a>
								<?php  endif; ?>
	   	    				 	<h4><?php the_title(); ?></h4>
	   	    				    <div class="archive-content__info archive-content__info--videos">
	   	    				 		<div class="archive-content__info-single archive-content__info-single--date-videos">
	   	    				 			<?php echo get_the_date(); ?>
	   	    				 		</div>
	   	    				 	</div>
	   	    					<?php the_excerpt(); ?>
	   	    				 </div>
	   	    			</div>
	   	    		</div>
                <?php
                  		endwhile;
                  	endif;
                     wp_reset_postdata();
                  ?>
	   	    	</div>

	   	    </div>
			<div class="large-4 columns">
				<!--  CATEGORY SIDEBAR -->
				<?php get_template_part( 'partials/content', 'category_videos' ); ?>
				<!--  /CATEGORY SIDEBAR -->
			</div>
	   </div>
       <?php get_template_part( 'partials/content', 'paginator' ); ?>
	</div>
</div>
<!-- REVEAL -->
 <?php
 global $wp_query;
 if ( $wp_query-> have_posts() ) : 
 	while ( $wp_query->have_posts() ) : 
 		$wp_query->the_post();
 	    $video = get_field('video_id');?>
		<div class="reveal small video-reveal" id="V<?=$video; ?>" data-reveal data-reset-on-close="true">
	   	  	<button class="close-button" data-close aria-label="Close modal" type="button">
	        	<img src="<?php bloginfo('template_url')?>/assets/img/close-modal.png" alt="">
	      	</button>
	      	<div class="vimeo-reveal" data-id="<?= $video; ?>"></div>
	      	<div class="video-reveal__content">
	      	<?php	$term_list = wp_get_post_terms($post->ID, 'category-videos', array("fields" => "all"));
										if($term_list): ?>
									    	<a href="<?php  echo get_category_link( $term_list[1]->term_id ); ?>">
									    			<?php echo $term_list[1]->name; ?>
									    	</a>
								<?php   else:?>
											<a href="<?php echo site_url(); ?>/videos/">All</a>
								<?php  endif; ?>
	      		<h1>  <?php the_title();?></h1>
	        	<?php the_excerpt(); ?>
	      	</div>
		</div>
   <?php
    endwhile;
 endif;
 wp_reset_postdata(); ?>
<!-- /REVEAL -->     