<?php 
$pizza_button = get_field('pizza_calculator_button', 'option'); 
$pizza_image = get_field('pizza_calculator_image', 'option'); 
?>
<div class="orange-bar">
	<div class="row">
		<div class="medium-12 large-10 columns large-centered orange-bar__content text-center">
			 <img src="<?= $pizza_image['url']; ?>" alt="<?= $pizza_image['alt']; ?>">
			 <a href="<?= $pizza_button['url']; ?>"><?= $pizza_button['title']; ?>&nbsp;»</a>
		</div>
	</div>
</div>