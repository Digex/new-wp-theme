	  	<div class="category-sidebar">
	  		<div class="category-sidebar__links category-sidebar__links--categories">
	  			<h4>Categories</h4>
	  			<ul>
	  			   <li>
		            <a href="<?php echo site_url(); ?>/culinary-blog/">All</a>
		          </li>
				   <?php $categories = get_categories( array(
					    'orderby' => 'name',
					    'order'   => 'ASC'
					) );
				   foreach ( $categories as $category ) {
				   	printf( '<li><a href="%1$s">%2$s</a></li>',
					        esc_url( get_category_link( $category->term_id ) ),
					        esc_html( $category->name )
					    );
					}
				?>
				</ul>
	  		</div>
	  		<div class="category-sidebar__links category-sidebar__links--archive">
	  			<h4>Archive</h4>
	  			<ul>
	  				<?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
	  			</ul>
	  		</div>
	  	</div>