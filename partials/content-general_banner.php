<?php
 $intro = get_field('intro');
 $banner_image = get_field('banner_image');
 $is_it_a_double_view_oven = get_field('is_it_a_double_view_oven') ? get_field('is_it_a_double_view_oven') : 0;
?>
<div class="general-banner <?php if ( is_singular( 'oven' ) ): ?> general-banner--single-oven<?php endif; ?> <?php if ( $is_it_a_double_view_oven): ?> general-banner--double-oven<?php endif; ?>" <?php if($banner_image != null): ?>style="background-image: url('<?php echo $banner_image['url'];?>')" <?php endif; ?>>
    <?php if ( is_singular( 'oven' ) ): ?>
	  <div class="row">
	  	<div class="small-12 columns" >
	  		<div class="general-banner__oven-back relative" >
	  		   		<a href="<?php echo site_url(); ?>/quiz"> <i class="fas fa-caret-left"></i> Change or update</a>
	  		   		<h3>
	  					Find the right oven for you
	  				</h3>
	  		</div>
	  	</div>
	  </div>
	<?php endif; ?>
	<?php 
	if(!$is_it_a_double_view_oven):
	?>
	<div class="row">
		<div class="small-12 columns text-center">
			<h1><?php the_title(); ?></h1>
			<?= $intro; ?>
		</div>
	</div>
	<?php endif; ?>
</div>