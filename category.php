<?php get_header() ;
$archive_culinary = get_field('archive_culinary', 'option'); 
?>
<div class="general-banner">
	<div class="row">
		<div class="small-12 columns text-center">
			<h1><?= $archive_culinary['title']?></h1>
			<?= $archive_culinary['content']?>
		</div>
	</div>
</div>
<div class="cloud-dark-pattern">
	<div class="archive-content">
	   <div class="row">
	   	    <div class="large-8 columns">
	   	    	<div class="archive-content__grid">
                <?php
                  global $wp_query;
 					if ( $wp_query-> have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
                    <div class="archive-content__single" >
	   	    			<div>
	   	    				<div class="archive-content__thumbnail">
	   	    					<?php
                            	if ( has_post_thumbnail() ): ?>
                                 <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
                               <?php 
                                else:  ?>
                                <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url' ) ?>/assets/img/press-default.jpg" /></a>
                               <?php endif; ?>	

	   	    				</div>
	   	    				 <div class="archive-content__excerpt">
	   	    				 	<h4><?php the_title(); ?></h4>
	   	    				    <div class="archive-content__info">
	   	    				 		<div class="archive-content__info-single archive-content__info-single--date">
	   	    				 			<?php echo get_the_date(); ?>
	   	    				 		</div>
	   	    				 		<div class="archive-content__info-single archive-content__info-single--category">
   	    				 			  <?php $categories = get_the_category();
										if ( ! empty( $categories ) ):
										    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
										endif; ?>
	   	    				 		</div>
	   	    				 	</div>
	   	    					<?php the_excerpt(); ?>
	   	    					<a href="<?php the_permalink(); ?>">Read More &raquo;</a>
	   	    				 </div>
	   	    			</div>
	   	    		</div>
                <?php
                  		endwhile;
                  	endif;
                     wp_reset_postdata();
                  ?>
	   	    	</div>

	   	    </div>
			<div class="large-4 columns">
				<!--  CATEGORY SIDEBAR -->
				<?php get_template_part( 'partials/content', 'category_sidebar' ); ?>
				<!--  /CATEGORY SIDEBAR -->
			</div>
	   </div>
       <?php get_template_part( 'partials/content', 'paginator' ); ?>
	</div>
</div>
<?php get_footer(); ?>