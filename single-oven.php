<?php 
  get_header();
  $is_it_a_double_view_oven = get_field('is_it_a_double_view_oven');
?>
<!--  BANNER -->
<?php get_template_part( 'partials/content', 'general_banner' ); ?>
<!--  /BANNER -->
<?php 
	if($is_it_a_double_view_oven):;
?>
 	<?php get_template_part( 'partials/content', 'double_oven' ); ?>
<?php else:  ?>
	<!-- PIZZA CALCULATOR -->
	<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
	<!-- /PIZZA CALCULATOR -->
	<?php get_template_part( 'partials/content', 'single_oven' ); ?>
<?php endif; ?>
<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->
<?php get_footer(); ?>
