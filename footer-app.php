<?php
  $footer = get_field('footer', 'option'); 
?>
<!-- FOOTER -->
<div id="footer" class="footer-app">
    <div class="row">
     <div id="bottom_footer">
         <div id="social_bar">
              <div class="social_icon">
                <a href="https://www.facebook.com/OventionOvens/" target="_blank" rel="nofollow">
                  <img src="<?php bloginfo('template_url') ?>/assets/img/facebook.png" alt="Facebook>"  class="hide-for-small">
                </a>
              </div>
              <div class="social_icon">
                <a href="https://twitter.com/OventionOvens" target="_blank" rel="nofollow">
                  <img src="<?php bloginfo('template_url') ?>/assets/img/twitter.png" alt="Twitter"  class="hide-for-small">
                </a>
              </div>
              <div class="social_icon">
                <a href="https://www.linkedin.com/company/2951065?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A1-1-1%2CtarId%3A1432744710307%2Ctas%3Aovention" target="_blank" rel="nofollow">
                  <img src="<?php bloginfo('template_url') ?>/assets/img/linkedin.png" alt="LinkedIn"  class="hide-for-small">
                </a>
              </div>

    </div>
      <div class="eight columns">
        <div  id="logo_footer" class="three columns no-padding ">
            <a href="<?php echo esc_url( home_url( '/' )); ?>"><img src="<?= $footer['footer_logo']['url']; ?>" alt="<?= $footer['footer_logo']['alt']; ?>"></a>
          </div>
        <div class="nine columns connect">
          <div id="hatco">
            <?= $footer['footer_copyright']; ?>
            <?= $footer['footer_info']; ?>
          </div>
      </div>
    </div>
      <div class="four columns">
        <div id="top_footer_menu" >
         <ul>
           <li><a href="<?php echo site_url(); ?>/ovention-ovens/">Catalog</a></li>
           <li><a href="<?php site_url() ?>/customer-care/">Support</a></li>
         </ul>
        </div>
        <div id="bottom_footer_menu">
          <ul>
            <li><a href="https://www3.hatcocorp.com/RegUser2/Login.jsp" target="_blank">Distributor Login</a></li>
          </ul>
        </div>
        <a href="http://ovention.kclcad.com/" target="_blank"><img id="kcl_logo" src="<?php echo bloginfo('template_url'); ?>/src/appstyles/images/KCL-Logo.png" alt=""></a>
      </div>
      <div class="clean"></div>
    </div>
  </div>
</div>
<!-- /FOOTER -->
</body>
<script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
<?php wp_footer(); ?>
<script type="text/javascript">_satellite.pageBottom();</script>
<!-- begin LeadformixChat code -->
<script type="text/javascript">
(function() {
var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
se.src = '//storage.googleapis.com/leadformixchat/js/14670904-f704-4bd3-872a-a97162db0806.js';
var done = false;
se.onload = se.onreadystatechange = function() {
if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
done = true;
/* Place your LeadformixChat JS API code below */
/* LeadformixChat.allowChatSound(true); Example JS API: Enable sounds for Visitors. */
}
};
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
})();
</script>
<!-- end LeadformixChat code -->
    <!-- LeadFormix -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://vlog.leadformix.com/" : "http://vlog.leadformix.com/");
<!--
bf_action_name = '';
bf_idsite = 9695;
bf_url = pkBaseURL+'bf/bf.php';
(function() {
var lfh = document.createElement('script'); lfh.type = 'text/javascript'; lfh.async = true;
lfh.src = pkBaseURL+'bf/lfx.js';
var s = document.getElementsByTagName('head')[0]; s.appendChild(lfh);
})();
//-->
</script>

<!-- /LeadFormix -->

<?php 
   get_template_part( 'partials/partial', 'trackingcode' ); 
?>

</html>