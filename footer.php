<?php
$footer = get_field('footer', 'option'); 
?>
<div class="footer">
	<div class="footer__top">
		<div class="row">
			<div class="medium-4 medium-centered large-3 large-uncentered columns footer__general">
				<a href="<?php echo esc_url( home_url( '/' )); ?>"><img src="<?= $footer['footer_logo']['url']; ?>" alt="<?= $footer['footer_logo']['alt']; ?>"></a>
				<div class="footer__info">
					<?= $footer['footer_info']; ?>
				</div>
				<div class="footer__social">
				<?php $rows = $footer['footer_social'];
						if($rows): 
						?>
							<?php foreach($rows as $row): 
								$social_url = $row['social_url'];
								$social_icon = $row['social_icon'];
							?>
							   <a href="<?= $social_url; ?>" target="_blank" rel="nofollow"><i class="fab <?=$social_icon; ?>"></i></a>
							<?php endforeach; ?>
				<?php	endif; ?>
				</div>
			</div>
			<div class="large-9 large-uncentered hide-for-small show-for-large columns footer__navigation">
				<ul >
	              <?php
	                $defaults = array(
	                  'theme_location'  => 'footer_menu',
	                  'menu'            => '',
	                  'container'       => '',
	                  'container_class' => '',
	                  'container_id'    => '',
	                  'menu_class'      => 'navigation',
	                  'menu_id'         => '',
	                  'echo'            => true,
	                  'fallback_cb'     => 'wp_page_menu',
	                  'before'          => '',
	                  'after'           => '',
	                  'link_before'     => '',
	                  'link_after'      => '',
	                  'items_wrap' => '%3$s',
	                  'depth'           => 0,
	                  'walker'        => new themeslug_walker_nav_menu_header
	                );
	              wp_nav_menu($defaults); ?>
	            </ul>
			</div>
		</div>
	</div>
	<div class="footer__bottom">
		<div class="row">
			<div class="large-6 large-centered columns">
				<div class="footer__copyright">
					<?= $footer['footer_copyright']; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
<?php wp_footer(); ?>
<!--[if !IE]><!-->
<!--<![endif]-->
</body>
<script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
<?php wp_footer(); ?>
<script type="text/javascript">_satellite.pageBottom();</script>
<!-- begin LeadformixChat code -->
<script type="text/javascript">
(function() {
var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
se.src = '//storage.googleapis.com/leadformixchat/js/14670904-f704-4bd3-872a-a97162db0806.js';
var done = false;
se.onload = se.onreadystatechange = function() {
if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
done = true;
/* Place your LeadformixChat JS API code below */
/* LeadformixChat.allowChatSound(true); Example JS API: Enable sounds for Visitors. */
}
};
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
})();
</script>
<!-- end LeadformixChat code -->
    <!-- LeadFormix -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://vlog.leadformix.com/" : "http://vlog.leadformix.com/");
<!--
bf_action_name = '';
bf_idsite = 9695;
bf_url = pkBaseURL+'bf/bf.php';
(function() {
var lfh = document.createElement('script'); lfh.type = 'text/javascript'; lfh.async = true;
lfh.src = pkBaseURL+'bf/lfx.js';
var s = document.getElementsByTagName('head')[0]; s.appendChild(lfh);
})();
//-->
</script>

<!-- /LeadFormix -->

<?php 
   get_template_part( 'partials/partial', 'trackingcode' ); 
?>
</html>
