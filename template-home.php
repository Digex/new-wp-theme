<?php
/*
*Template Name: Home
*/

get_header();
$home_banner = get_field('home_banner');
$first_section = get_field('first_section');
$schedule_a_demo = get_field('schedule_a_demo');
$home_app_section = get_field('home_app_section');
$home_slider = get_field('home_slider');
?>
<!--  HOME BANNER -->
<div class="home-banner">
	<div class="row">
		<div class="large-10 small-centered columns home-banner__text">
			<h1>
				<?= $home_banner['home_banner_title']; ?>
			</h1>
			<h3>
				<?= $home_banner['home_banner_secondary']; ?>
			</h3>
		</div>
	</div>
	<div class="row">
		<div class="large-4 columns home-banner__oven-info">
		<?php $rows = $home_banner['home_banner_buttons'];
					if($rows): 
					?>
						<?php foreach($rows as $row): 
							$buttondata = $row['home_banner_button_content'];
							$typebutton = $row['home_banner_button_color'];
						?>
							<a href="<?= $buttondata['url']; ?>" class="home-banner__button home-banner__button--<?= $typebutton; ?> ovention-button ovention-button--<?= $typebutton; ?>" target="<?= $buttondata['target']; ?>"><?= $buttondata['title']; ?> &raquo;</a>
						<?php endforeach; ?>
			<?php	endif; ?>
			<div class="home-banner__stats">
				<?= $home_banner['home_banner_stats']; ?>
			</div>
		</div>
		<div class="large-8 columns">
		    <div class="home-banner__slider">
	    		<?php $rows = $home_slider['home_slider'];
					if($rows): 
					?>
						<?php foreach($rows as $row): 
							$home_slider_image = $row['home_slider_image'];
						?>
						<div class="home-banner__single-slider">
					 		<img src="<?= $home_slider_image['url']; ?>" alt="<?= $home_slider_image['alt']; ?>">
					 	</div>
						<?php endforeach; ?>
				<?php endif; ?>
		    </div>
			 <div class="home-banner__controls">
			 	<div class="home-banner__controls--prev">
			 		<i class="fas fa-caret-left"></i>
			 	</div>
			 	<div class="home-banner__legend">
				 	<?php $rows = $home_slider['home_slider'];
						if($rows): 
						?>
							<?php foreach($rows as $row): 
								$home_slider_caption = $row['home_slider_caption'];
							?>
							<span><?= $home_slider_caption; ?></span>
							<?php endforeach; ?>
					<?php endif; ?>	
			 	</div>
			 	<div class="home-banner__controls--next">
			 		<i class="fas fa-caret-right"></i>
			 	</div>
			 </div>
		</div>
	</div>
	<img src="<?php bloginfo('template_url');?>/assets/img/table.png" alt="" class="home-banner__table">
</div>
<!--  /HOME BANNER -->

<!-- PIZZA CALCULATOR -->
<?php get_template_part( 'partials/content', 'pizza_calculator' ); ?>
<!-- /PIZZA CALCULATOR -->

<!--  FIRTS SECTION -->
<div class="main-home cloud-pattern">
	<div class="row">
		<div class="small-12 columns main-home__intro">
			<h2>
				<?= $first_section['first_section_title']; ?>
			</h2>
			<div class="main-home__text">
				<?= $first_section['first_section_content']; ?>
			</div>
			<a href="<?= $first_section['first_section_button']['url']; ?>" class="main-home__button--intro ovention-button ovention-button--gray"><?= $first_section['first_section_button']['title']; ?> &raquo;</a>		
		</div>
	</div>
	<!--  DYNAMIC OVENS -->
	<div class="row">
	<?php
	   $args = array(
		    'post_type'      => 'page',
		    'posts_per_page' => 2,
		    'meta_key' => '_wp_page_template',
        	'meta_value' => 'template-ovens.php',
		    'order'          => 'ASC',
		    'orderby'        => 'rand'
		 );
	   $query = new WP_Query( $args );
        if($query -> have_posts() ):
            while ($query -> have_posts()):
                  $query -> the_post();
                  $id = get_the_id();
                  $mini_data              = (get_field('mini_data',$id)) ? get_field('mini_data',$id) : '' ;
              ?>
		<div class="small-11 small-centered medium-7  large-6 large-uncentered columns main-home__oven">
			<img src="<?=$mini_data['mini_intro_image']['url']; ?>" alt="<?=$mini_data['mini_intro_image']['alt']; ?>">
			<a href="<?php the_permalink(); ?>"><h3><?=$mini_data['mini_title']?></h3></a>
			<div class="main-home__description">
				<?=$mini_data['mini_description']?>
			</div>
			<a href="<?php the_permalink(); ?>" class="main-home__button--oven ovention-button ovention-button--colors">Learn more &raquo;</a>
		</div>
		 <?php
            endwhile;
        endif; ?>
	</div>
	<!--  /DYNAMIC OVENS -->
</div>
<!--  /FIRTS SECTION -->

<!--  SCHEDULE A DEMO -->
<div class="demo-bar">
	<div class="row">
		<div class="medium-8 large-8 columns demo-bar__text">
			<h2>
				<?= $schedule_a_demo['schedule_a_demo_title']; ?>
			</h2>
			<?= $schedule_a_demo['schedule_a_demo_content']; ?>
			<a href="<?= $schedule_a_demo['schedule_a_demo_button']['url']; ?>" class="demo-bar__button ovention-button ovention-button--orange"><?= $schedule_a_demo['schedule_a_demo_button']['title']; ?> &raquo;</a>
		</div>
		<div class="medium-4 large-4 columns demo-bar__image">
			<img src="<?= $schedule_a_demo['schedule_a_demo_image']['url']; ?>" alt="<?= $schedule_a_demo['schedule_a_demo_image']['alt']; ?>">
		</div>
	</div>
</div>
<!--  /SCHEDULE A DEMO -->

<!--  HOME APP -->
<div class="home-app full-cloud-pattern hide-for-small show-for-large">
	<div class="row">
		<div class="large-7 columns home-app__text">
			<h2>
				<?= $home_app_section['home_app_section_title']; ?>
			</h2>
			<?= $home_app_section['home_app_section_content']; ?>
			<a href="<?= $home_app_section['home_app_section_button']['url']; ?>" class="home-app__button ovention-button ovention-button--colors"><?= $home_app_section['home_app_section_button']['title']; ?> &raquo;</a>
		</div>
		<div class="large-5 columns home-app__image">
			<img src="<?= $home_app_section['home_app_section_image']['url']; ?>" alt="<?= $home_app_section['home_app_section_image']['alt']; ?>">
		</div>
	</div>
</div>
<!--  /HOME APP -->

<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->
<?php get_footer(); ?>
