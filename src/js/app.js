jQuery(document).foundation();


(function($){
  $(document).ready(function(){
      $('.oven-ribbons__close').on('click',function(){
          console.log('click');
          $('.oven-ribbons').hide();
      });
  });
})(jQuery);

(function($){
   var $window = $(window);
    function checkWidth() {
        var windowsize = $window.width();
        if (windowsize > 1024) {
          $('.archive-content__grid').masonry({
            itemSelector: '.archive-content__single',
            columnWidth: 370
          }); 
        }else{
          $('.archive-content__grid').masonry('destroy');
        }
    }
	$(document).ready(function(){
    $('.featured-slider').slick({
        arrows: false,
        autoplay: true,
        dots: true,
        adaptiveHeight: true,
        appendDots:$('.dots_custom'),
        autoplaySpeed: 4000,
    });
		$('.top-header__testimonies').slick({
			arrows: false,
			autoplay: true,
  			autoplaySpeed: 4000,
		});
    $('.intro-oven__reviews-content').slick({
      speed: 200,
      prevArrow: '.intro-oven__reviews-arrow--prev',
      nextArrow: '.intro-oven__reviews-arrow--next',
    });
		$('.home-banner__slider').slick({
			asNavFor: '.home-banner__legend',
			fade: true,
			arrows: false,
			autoplay: true,
  			autoplaySpeed: 5000,
		});
		$(".home-banner__legend").slick({
		  asNavFor: '.home-banner__slider',
		  speed: 200,
		  fade: true,
		  prevArrow: '.home-banner__controls--prev',
		  nextArrow: '.home-banner__controls--next',
		});
    /*SUBMIT QUIZ*/
    var Os = getOS();
    if(Os == 'iOS')
    {
      $('#ios-button').show();
    }
    else 
    {
      if(Os == 'Android')
      {
        $('#android-button').show();
      }
    }
    setTimeout(function(){if($('.quiz input[id*="gform_submit_button"]').is(':visible'))
    {
      $('.gform_page_footer').css('left','-156000px');
      $('input[id*="gform_submit_button"]').click();
    }},100);
    //SUBMIT THE QUIZ
      $('.reload').on('click',function(event){
          var origin = window.location.origin;
            var pathname = window.location.pathname;
            location.href= origin + pathname;
         });
	});
  $(window).on('load', function(){
      checkWidth();
  });	
  $(window).resize(checkWidth);
})(jQuery);


  /* GET OS */
  function getOS() {
     var platform = "Unknown OS";
     var userAgent = navigator.userAgent || navigator.vendor || window.opera;

     if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ){
       platform = 'iOS';
     }
     else if( userAgent.match( /Android/i ) ){
       platform = 'Android';
     }
     else{
       if (navigator.appVersion.indexOf("Win")!=-1){ 
         platform="Windows";
       }
       else if (navigator.appVersion.indexOf("Mac")!=-1){ 
         platform="Mac";
       }
     }
     return platform;
   }


document.addEventListener("DOMContentLoaded",
    function() {
      var div, n, v = document.getElementsByClassName("youtube-player");
      for (n = 0; n < v.length; n++) {
        div = document.createElement("div");
        div.setAttribute("data-id", v[n].getAttribute('data-id'));
        div.innerHTML = labnolThumb(v[n].getAttribute('data-id'));
        div.onclick = labnolIframe;
        v[n].appendChild(div);
      }

      var div, n, v = document.getElementsByClassName("vimeo-player");
      for (n = 0; n < v.length; n++) {
        div = document.createElement("div");
        div.setAttribute("data-id", v[n].getAttribute('data-id'));
        div.innerHTML = labnolThumbvideo(v[n].getAttribute('data-id'));
        div.onclick = labnolIframeVimeo;
        v[n].appendChild(div);
      }
      
      var div, n, v = document.getElementsByClassName("vimeo-thumb");
      for (n = 0; n < v.length; n++) {
        div = document.createElement("div");
        div.setAttribute("data-id", v[n].getAttribute('data-id'));
        div.innerHTML = labnolThumbvideo(v[n].getAttribute('data-id'));
        v[n].appendChild(div);
      }
      var div, n, v = document.getElementsByClassName("vimeo-reveal");
      for (n = 0; n < v.length; n++) {
        div = document.createElement("div");
        div.setAttribute("data-id", v[n].getAttribute('data-id'));
        var attributeID = v[n].getAttribute('data-id');
        var iframe = document.createElement("iframe");
        var embed = "https://player.vimeo.com/video/ID";
        iframe.setAttribute("src", embed.replace("ID", attributeID));
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "1");
        v[n].appendChild(iframe);
      }
  });

  

  /*VIMEO FUNCTIONS*/
  function labnolIframeVimeo() {
      var iframe = document.createElement("iframe");
      var embed = "https://player.vimeo.com/video/ID?autoplay=1";
      iframe.setAttribute("src", embed.replace("ID", this.getAttribute('data-id')));
      iframe.setAttribute("frameborder", "0");
      iframe.setAttribute("allowfullscreen", "1");
      this.parentNode.replaceChild(iframe, this);
  }
  function labnolIframeVimeoReveal() {
      var iframe = document.createElement("iframe");
      var embed = "https://player.vimeo.com/video/ID";
      iframe.setAttribute("src", embed.replace("ID", this.getAttribute('data-id')));
      iframe.setAttribute("frameborder", "0");
      iframe.setAttribute("allowfullscreen", "1");
      this.parentNode.replaceChild(iframe, this);
  }
  function labnolThumbvideo(id) {
      var play = '<div class="play"></div>';
      return play;
  }
  /*YOUTUBE FUNCTIONS*/
  function labnolThumb(id) {
      var thumb = '<img src="https://i.ytimg.com/vi/ID/sddefault.jpg">',
          play = '<div class="play"></div>';
      return thumb.replace("ID", id) + play;
  }
  function labnolIframe() {
      var iframe = document.createElement("iframe");
      var embed = "https://www.youtube.com/embed/ID?autoplay=1&rel=0";
      iframe.setAttribute("src", embed.replace("ID", this.getAttribute('data-id')));
      iframe.setAttribute("frameborder", "0");
      iframe.setAttribute("allowfullscreen", "1");
      this.parentNode.replaceChild(iframe, this);
  }
 jQuery(document).ready(function(){
  jQuery('.vimeo-player').each(function(index){
  var jQuerythis  = this,
      idElement = jQuery(this).attr('data-id');

  jQuery.ajax({
          type:'GET',
          url: 'http://vimeo.com/api/v2/video/' + idElement  + '.json',
          jsonp: 'callback',
          dataType: 'jsonp',
          success: function(data){

              var video = data[0];
               jQuery('<img/>' ,{
                   src:video.thumbnail_large
                }).prependTo(jQuerythis);
          }
      });
  });
  jQuery('.vimeo-thumb').each(function(index){
  var jQuerythis  = this,
      idElement = jQuery(this).attr('data-id');
  jQuery.ajax({
          type:'GET',
          url: 'http://vimeo.com/api/v2/video/' + idElement  + '.json',
          jsonp: 'callback',
          dataType: 'jsonp',
          success: function(data){

              var video = data[0];
               jQuery('<img/>' ,{
                   src:video.thumbnail_large
                }).prependTo(jQuerythis);
          }
      });
  });
 });

(function($){
$(document).ready(function($){
  ajaxInformation();
  $("#ovenpost").on("change", ajaxInformation);
  $("#pizza").on("change", ajaxInformation)

});

function ajaxInformation(){
  var ovenpost = $("#ovenpost").val(),
      pizza = $("#pizza").val();

  if(ovenpost != 0 && pizza != 0){
    ajaxPizza(ovenpost, pizza);
  }
}



function ajaxPizza(p, pizza){
  var data = {

    'action': 'ajaxPost',
    "p": p,
    "pizza": pizza
  };

  $.ajax({
    type: 'post',
    url : ajaxurl,
    data: data,
     success: function(response){
        $(".responsehtml").html(response);
     }

  });
}
})(jQuery);

