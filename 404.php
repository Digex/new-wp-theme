<?php
get_header();
?>

<div class="general-banner" style="padding: 20rem 0;" >
	<div class="row">
		<div class="small-12 columns text-center">
			<h1>404</h1>
			<p>Something is wrong. The page you are looking for was moved, removed, renamed or might never existed.</p>
		</div>
	</div>
</div>
<!--  BLOG BAR -->
<?php get_template_part( 'partials/content', 'culinary_bar' ); ?>
<!--  /BLOG BAR -->

<?php get_footer(); ?>
