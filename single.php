<?php get_header() ?>
<div class="single-blog cloud-dark-pattern">
	<div class="row">
	  <div class="large-8 columns">
		  <div class="single-blog__content">
		        <div class="single-blog__banner">
		        	<?php the_post_thumbnail('full') ?>
		        </div>
		        <div class="single-blog__post">
		           <div class="single-blog__breadcrums">
		           	<a href="<?php echo site_url(); ?>">Home</a> / <a href="<?php echo site_url(); ?>/culinary-blog/">Culinary Blog</a>
		           </div>
		           <h1><?php the_title(); ?></h1>
		           <div class="single-blog__info">
		            <div class="single-blog__singular-info single-blog__singular-info--date">
		            	<?php echo get_the_date(); ?>
		            </div>
		            <span class="single-blog__separator">|</span>
		            <div class="single-blog__singular-info single-blog__singular-info--category">
		              <?php $categories = get_the_category();
						if ( ! empty( $categories ) ):
						    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
						endif; ?>
		            </div>
		           	<?php if ( is_active_sidebar( 'social_widget' ) ) : ?>
		           		<span class="single-blog__separator">|</span>
					<div class="single-blog__singular-info single-blog__singular-info--social">
						<span>Share</span><?php dynamic_sidebar( 'social_widget' ); ?>
					</div>
					<?php endif; ?>
		           </div>
			       <?php if ( have_posts() ) : 
							while ( have_posts() ) : the_post();
								the_content();
							endwhile; 
						  else: ?>
							<p>Sorry, no posts matched your criteria.</p>
					<?php endif; ?>
					<div class="single-blog__bottom-share">
						<?php if ( is_active_sidebar( 'social_widget' ) ) : ?>
								<span>Share</span><?php dynamic_sidebar( 'social_widget' ); ?>
						<?php endif; ?>
					</div>
					<div class="single-blog__related">
						<h3>Related</h3>
						<?php
							$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 5, 'post__not_in' => array($post->ID) ) );
							if( $related ) foreach( $related as $post ) {
							setup_postdata($post); ?>
							 <ul> 
							        <li>
							        <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
							        </li>
							    </ul>   
							<?php }
							wp_reset_postdata(); ?>
					</div>
		        </div>
		  </div>
	  </div>
	  <div class="large-4 columns">
		<!--  CATEGORY SIDEBAR -->
		<?php get_template_part( 'partials/content', 'category_sidebar' ); ?>
		<!--  /CATEGORY SIDEBAR -->
	  </div>
	</div>
</div>
<?php get_footer(); ?>