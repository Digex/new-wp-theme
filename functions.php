<?php
add_theme_support( 'post-thumbnails' );

/************************
*EXTENDS NAV
*************************/
register_nav_menus( array(
  'header_main_menu' => 'Header Menu',
  'header_top_menu' => 'Header Top Menu',
  'footer_menu' => 'Footer Menu'
));

/************************
*LOAD CSS AND Js
*************************/
function theme_styles(){
  if( !(is_post_type_archive('recipes') or is_page_template('template-app_login.php') or  is_page_template('template-app-registration.php'))){
    wp_register_style( 'styles', get_template_directory_uri() . '/assets/css/app.min.css', array(), 'v.2.1', 'all' );
    wp_enqueue_style( 'styles' );
     wp_enqueue_script('jquery');
  }
 
}
add_action('wp_enqueue_scripts', 'theme_styles');

function theme_scripts(){
  if( !(is_post_type_archive('recipes') or is_page_template('template-app_login.php') or  is_page_template('template-app-registration.php'))){
  wp_enqueue_script('theme-scripts', get_template_directory_uri() . '/assets/js/app.min.js', array('jquery'), 'v.1.1', true);
}


}
add_action('wp_footer', 'theme_scripts');



/***************************
* SETTINGS PAGE
****************************/
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-settings'
  ));

  acf_add_options_sub_page(array(
      'title' => 'Header',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
  acf_add_options_sub_page(array(
      'title' => 'Pizza Calculator',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
  acf_add_options_sub_page(array(
      'title' => 'Culinary Bar',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
  acf_add_options_sub_page(array(
      'title' => 'Footer',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
  acf_add_options_sub_page(array(
      'title' => 'Archive Videos',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
  acf_add_options_sub_page(array(
      'title' => 'Archive Culinary Blog',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
}

/***************************
*EXTENDS NAV
****************************/
$file = TEMPLATEPATH."/inc/extends-nav.php";
if(file_exists($file)){
    require_once($file);
}

/*******************
*POST TYPE
********************/
$file = TEMPLATEPATH."/inc/post-type-oven/post-type-oven.php";
if(file_exists($file)){
    require_once($file);
}
$file = TEMPLATEPATH."/inc/post-type-videos/post-type-videos.php";
if(file_exists($file)){
    require_once($file);
}
$file = TEMPLATEPATH."/inc/post-type-pizza-calculator/post-type-pizza-calculator.php";
if(file_exists($file)){
    require_once($file);
}
/*****************
*SOCIAL WIDGETS
*******************/
function social_widgets_init() {

  register_sidebar( array(
    'name'          => 'Social Widget',
    'id'            => 'social_widget',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
  ) );

}
add_action( 'widgets_init', 'social_widgets_init' );

/*******************
*QUIZ 
********************/
add_action( 'gform_after_submission', 'after_submission_handler',10,2 );
  function after_submission_handler( $entry, $form ) { 
  $site_url = network_site_url( '/' );
  setcookie("FirstAnswer", rgar($entry,'2'), time()+3600, "/");
  setcookie("SecondAnswer",rgar($entry,'5'), time()+3600, "/");
  $one = rgar($entry,'9');
  $two = rgar($entry,'12');
  $three = rgar($entry,'14');
  $four = rgar($entry,'15');
  $five = rgar($entry,'17');
  $six = rgar($entry,'10');
 if($one == '16” round pizza pan' || $two == 'Somewhat important I’ll dedicate the space for the right solution.' || $two == 'Not a strong consideration  I have plenty of space.'):
    header("Location: ".$site_url."oven/m1718/");
    die();
  elseif($three == 'Very important I’ve got a shoebox to work with- small is critical.'):
    header("Location: ".$site_url."oven/m360/");
    die();
  elseif($three == 'Somewhat important  I’ll dedicate the space for the right solution.' || $three == 'Not a strong consideration I have plenty of space.'):
    header("Location: ".$site_url."oven/m1313/");
    die();
  elseif($two == 'Very important  I’ve got a shoebox to work with- small is critical.'):
    header("Location: ".$site_url."oven/m360/");
    die();
  elseif($four == 'No one product only, over and over.' || $five == 'No one product only, over and over.'):
    header("Location: ".$site_url."oven/c2000/");
    die();
  elseif($six == 'Something larger'):
    header("Location: ".$site_url."oven/c2600/");
    die();
  elseif($four== 'Somewhat I have other options, but I’d like flexibility' || $four== 'Absolutely  I need to do multiple products to make my business work' || $five == 'Somewhat I have other options, but I’d like flexibility' || $five == 'Absolutely I need to do multiple products to make my business work'):
    header("Location: ".$site_url."oven/s2000/");
    die();
  elseif($six == '¼ size sheet pan' || $six == '12” round pizza pan'):
    header("Location: ".$site_url."oven/s1200/");
    die();
  endif;

}
/*******************
* MOVE SCRIPTS TO FOOTER
********************/
/*function scripts_footer() {
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);
 
    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'scripts_footer' );*/

  /*****************
  * PAGINATION
  ******************/
  function pagination($prev = 'Prev', $next = 'Next') {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    $pagination = array(
        'base' => @add_query_arg('paged','%#%'),
        'format' => '',
        'total' => $wp_query->max_num_pages,
        'current' => $current,
        'prev_text' => __($prev),
        'next_text' => __($next),
        'type' => 'plain'
);
   
    echo paginate_links( $pagination );
};

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// Load Posts
function ajaxPizzaCalculator() {
  locate_template('/calculator-pizza-loop.php', TRUE, FALSE);
  die();
}

add_action('wp_ajax_nopriv_ajaxPost','ajaxPizzaCalculator');
add_action('wp_ajax_ajaxPost','ajaxPizzaCalculator');

/*ADD CUSTOM COLUMNS ON CUSTOM POSTS*/
add_filter( 'manage_edit-recipes_columns', 'my_edit_recipes_columns' ) ;

function my_edit_recipes_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Recipe' ),
        'course' => __( 'Course' ),
        'dish_type' => __( 'Dish Type' ),
        'image' => __( 'Image' ),
        'recipe_type' => __( 'Owner' )
    );

    return $columns;
}

/***************************************
CREATE CUSTOM COLUMNS ON RECIPES CUSTOM POST
*****************************************/
/*CREATE COLUMNS*/
add_action( 'manage_recipes_posts_custom_column', 'my_manage_recipes_columns', 10, 2 );
function my_manage_recipes_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'course' :
            $terms = get_the_terms( $post_id, 'category_recipes' );

            /* Get the post meta. */
            if ( $terms && ! is_wp_error( $terms ) ) : 

                $draught_links = array();

                foreach ( $terms as $term ) {
                    $draught_links[] = $term->name;
                }
                                    
                $on_draught = join( ", ", $draught_links );
                echo $on_draught;
            endif;    
            break;

        case 'dish_type' :

            /* Get the post meta. */
            $dish_type = get_field("dish_type", $post_id);

            echo ( $dish_type != "" ) ? $dish_type : "";
            break;

        case 'image' :

            /* Get the post meta. */
            $image = get_field("image", $post_id,false);
            echo wp_get_attachment_image( $image, 'thumbnail' );
            //echo (!empty($image['sizes']['thumbnail']) ) ? "<img src='".$image['sizes']['thumbnail']."' alt=''>" : "";
            

            break;

        case 'recipe_type' :

            /* Get the post meta. */
            $recipe_owner = get_field("recipe_type", $post_id);

            
            echo ($recipe_owner == "community") ? "Community" : "Ovention Approved" ; 
            break;    

        /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}

/*SORTABLE COLUMNS*/
/*Configure filter: manage_edit-{post_type}_sortable_columns’*/
add_filter( 'manage_edit-recipes_sortable_columns', 'my_recipes_sortable_columns' );
function my_recipes_sortable_columns( $columns ) {

    $columns['course']      = 'course';
    $columns['dish_type']   = 'dish_type';
    $columns['recipe_type'] = 'recipe_type';


    return $columns;
}

/***************************************
ADD SELECT FILTER ON RECIPES CUSTOM POST
*****************************************/
/*CREATE SELECT */
add_action( 'restrict_manage_posts' , 'filter_by_type' );
function filter_by_type()
{
    // Only apply the filter to our specific post type
    global $typenow;
    if( $typenow == 'recipes' )
    {
        echo "<select name='filter_owner'>";
        echo "<option value=''>All recipes</option>";
        echo "<option value='community'> Community</option>";
        echo "<option value='ovention_approved'> Ovention Approved</option>";
        echo "</select>";
    }
}

/*ADD VALUES TO QUERY */
add_filter( 'parse_query', /*array( $this, */'modify_filter_owner' /*)*/ );
function modify_filter_owner( $query )
{
    global $typenow;
    global $pagenow;

    if( $pagenow == 'edit.php' && $typenow == 'recipes' && isset($_GET['filter_owner']) )
    {
        $query->query_vars['meta_key']   = 'recipe_type';
        $query->query_vars['meta_value'] = 'community';

        if( $_GET['filter_owner'] == 'community' ){
            $query->query_vars['meta_compare'] = '==';
        } else {
            $query->query_vars['meta_compare'] = '!=';
        }
    }
}
